FROM node:alpine

RUN mkdir /sr
ADD ./package.json /sr
ADD ./yarn.lock /sr

WORKDIR /sr
RUN yarn install  --frozen-lockfile  && yarn cache clean
ADD . /sr

RUN yarn build