import { Redis } from "ioredis";

export interface EventPureConfig {
  syncronous: boolean; //
  name: string; // Name of the event log on redis
}

export interface EventPureConstructorOpts<State> {
  initialState: State;
  config: EventPureConfig;
  redis?: Redis;
}

class EventPure<
  State,
  EventMap extends Record<string, { type: EvKind }>,
  EvKind extends keyof EventMap
> {
  private config: EventPureConfig;
  public state: State; // State built from events in order. use {} if you don't want any state
  private redis?: Redis; // Optionally back event log with redis

  private eventListeners: Partial<
    Record<EvKind, Array<(e: EventMap[EvKind], state: State) => State>>
  >;

  private preHandlers: Partial<
    Record<EvKind, Array<(e: EventMap[EvKind], state: State) => Promise<void>>>
  >;

  private postHandlers: Partial<
    Record<EvKind, Array<(e: EventMap[EvKind], state: State) => Promise<void>>>
  >;

  constructor({
    initialState,
    config,
    redis
  }: EventPureConstructorOpts<State>) {
    this.state = initialState;
    this.config = config;
    this.redis = redis;
    this.eventListeners = {};
    this.preHandlers = {};
    this.postHandlers = {};
  }

  /**
   * Post an event to be processed. All event handlers are syncronous
   * pre/post handlers can be async, and are returned.
   * @param e Event to be handled
   */
  public postEvent<Kind extends EvKind>(
    e: EventMap[Kind]
  ): { prePromises: Promise<void>[]; postPromises: Promise<void>[] } {
    const evKind = e.type;
    const listeners: Array<(e: EventMap[Kind], state: State) => State> =
      this.eventListeners[evKind] ?? [];
    const preHanders: Array<(
      e: EventMap[Kind],
      state: State
    ) => Promise<void>> = this.preHandlers[evKind] ?? [];
    const postHandlers: Array<(
      e: EventMap[Kind],
      state: State
    ) => Promise<void>> = this.postHandlers[evKind] ?? [];
    if (!listeners && !preHanders && !postHandlers) {
      return {
        prePromises: [],
        postPromises: []
      };
    }

    const preState = this.state;

    for (const listener of listeners) {
      this.state = listener(e, this.state);
    }
    const postState = this.state;

    const prePromises = preHanders.map(fn => fn(e, preState));
    const postPromises = postHandlers.map(fn => fn(e, postState));
    return {
      prePromises,
      postPromises
    };
  }

  /**
   * like postEvent, but awaits all pre/post handlers
   * @param e Event to be handled
   */
  public async postEventAsync<Kind extends EvKind>(
    e: EventMap[Kind]
  ): Promise<void> {
    const { prePromises, postPromises } = this.postEvent(e);
    await Promise.all([...prePromises, ...postPromises]);
  }

  /**
   * Attach handlers that modify the game state for an event
   * they should not modify the state passed in, but return the updated state.
   * @param fn
   */
  public async addEventHandler<
    Kind extends EvKind,
    Event extends EventMap[Kind]
  >(evKind: Kind, fn: (e: Event, state: State) => State) {
    const listeners: Array<(e: Event, state: State) => State> =
      this.eventListeners[evKind] ?? [];
    if (!this.eventListeners[evKind]) {
      this.eventListeners[evKind] = listeners as any;
    }
    listeners.push(fn);
  }

  /**
   * Attach handlers that work with game state before an event occurs
   * do not change the game state!
   * @param fn
   */
  public async addPreHandler<Kind extends EvKind, Event extends EventMap[Kind]>(
    evKind: Kind,
    fn: (e: Event, state: State) => Promise<void>
  ) {
    const listeners: Array<(e: Event, state: State) => Promise<void>> =
      this.preHandlers[evKind] ?? [];
    if (!this.preHandlers[evKind]) {
      this.preHandlers[evKind] = listeners as any;
    }
    listeners.push(fn);
  }

  /**
   * Attach handlers that work with game state after an event occurs
   * do not change the game state!
   * @param fn
   */
  public async addPostHandler<
    Kind extends EvKind,
    Event extends EventMap[Kind]
  >(evKind: Kind, fn: (e: Event, state: State) => Promise<void>) {
    const listeners: Array<(e: Event, state: State) => Promise<void>> =
      this.postHandlers[evKind] ?? [];
    if (!this.postHandlers[evKind]) {
      this.postHandlers[evKind] = listeners as any;
    }
    listeners.push(fn);
  }
}

export default EventPure;
