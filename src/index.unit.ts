import EventPure, { EventPureConfig } from ".";
import faker from "faker";
import { TestEventMap, TestEventKinds, IncEvent } from "./tests/events";
import { TestState } from "./tests/state";
import { incEventHandler, counterEventHandler } from "./tests/handlers";
import { fakerSetup } from "./tests/faker";

describe("EventPure Tests", () => {
  beforeEach(fakerSetup);
  const initialState: TestState = { counter: 0 };

  const config: EventPureConfig = {
    syncronous: true,
    name: faker.random.uuid()
  };

  let eventPure: EventPure<TestState, TestEventMap, TestEventKinds>;
  beforeEach(() => {
    eventPure = new EventPure<TestState, TestEventMap, TestEventKinds>({
      initialState,
      config
    });
  });

  it("setting initial state", () => {
    expect(initialState).toEqual(eventPure.state);
  });

  it("handle increment event", () => {
    eventPure.addEventHandler("IncEvent", incEventHandler);
    eventPure.postEvent({ type: "IncEvent" });
    expect({
      counter: 1
    }).toEqual(eventPure.state);
  });

  it("should ignore events without handlers", () => {
    eventPure.postEvent({ type: "IncEvent" });
    expect({
      counter: 0
    }).toEqual(eventPure.state);
  });

  it("handle many increment event", () => {
    eventPure.addEventHandler("IncEvent", incEventHandler);
    const count = faker.random.number({ min: 2, max: 10 });
    for (let i = 0; i < count; i++) {
      eventPure.postEvent({ type: "IncEvent" });
    }
    expect({
      counter: count
    }).toEqual(eventPure.state);
  });

  it("handle counter event", () => {
    const amount = 100;

    eventPure.addEventHandler("IncEvent", (e, s) => s);
    eventPure.addEventHandler("IncEvent", incEventHandler);
    eventPure.addEventHandler("CounterEvent", counterEventHandler);

    eventPure.postEvent({ type: "IncEvent" });
    eventPure.postEvent({ type: "IncEvent" });
    eventPure.postEvent({ type: "CounterEvent", amount });
    expect({
      counter: amount
    }).toEqual(eventPure.state);
  });

  it("should handle pre handlers", async () => {
    let preCounter = -1;
    eventPure.addPreHandler(
      "IncEvent",
      async (e: IncEvent, state: TestState) => {
        preCounter = state.counter;
      }
    );
    eventPure.addEventHandler("IncEvent", incEventHandler as any);

    await eventPure.postEventAsync({ type: "IncEvent" });

    expect(preCounter).toEqual(0);
    expect({
      counter: 1
    }).toEqual(eventPure.state);
  });

  it("should handle post handlers", async () => {
    let postCounter = -1;
    eventPure.addPostHandler(
      "IncEvent",
      async (e: IncEvent, state: TestState) => {
        postCounter = state.counter;
      }
    );
    eventPure.addEventHandler("IncEvent", incEventHandler as any);

    await eventPure.postEventAsync({ type: "IncEvent" });

    expect(postCounter).toEqual(1);
    expect({
      counter: 1
    }).toEqual(eventPure.state);
  });

  it("should handle both pre/post handlers", async () => {
    let preCounter = -1;
    let postCounter = -1;
    eventPure.addPreHandler(
      "IncEvent",
      async (e: IncEvent, state: TestState) => {
        preCounter = state.counter;
      }
    );
    eventPure.addPostHandler(
      "IncEvent",
      async (e: IncEvent, state: TestState) => {
        postCounter = state.counter;
      }
    );
    eventPure.addEventHandler("IncEvent", incEventHandler as any);

    await eventPure.postEventAsync({ type: "IncEvent" });

    expect(preCounter).toEqual(0);
    expect(postCounter).toEqual(1);
    expect({
      counter: 1
    }).toEqual(eventPure.state);
  });
});
