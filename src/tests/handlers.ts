import { TestState } from "./state";
import { CounterEvent, IncEvent } from "./events";

export function incEventHandler(e: IncEvent, state: TestState): TestState {
  return {
    ...state,
    counter: state.counter + 1
  };
}

export function counterEventHandler(
  e: CounterEvent,
  state: TestState
): TestState {
  return {
    ...state,
    counter: e.amount
  };
}
