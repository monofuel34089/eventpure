export interface CounterEvent {
  type: "CounterEvent";
  amount: number;
}

export interface IncEvent {
  type: "IncEvent";
}

export interface TestEventMap {
  [key: string]: CounterEvent | IncEvent;
}

export type TestEventKinds = keyof TestEventMap;
