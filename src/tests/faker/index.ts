import faker from "faker";

export function fakerSetup() {
  let seed = Math.ceil(Math.random() * 1000) + 1;
  if (process.env.SEED) {
    seed = Number(process.env.SEED);
  }
  faker.seed(seed);
  console.log(`faker seed: ${seed}`);
}
