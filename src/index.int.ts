import EventPure, { EventPureConfig } from ".";
import IORedis from "ioredis";
import faker from "faker";

describe("EventPure Tests", () => {
  const config: EventPureConfig = {
    syncronous: true,
    name: faker.random.uuid()
  };
  it("passing redis config", async () => {
    // TODO set port/host through env var for tests
    const redis = new IORedis();

    redis.multi();

    const initialState = {};
    const eventPure = new EventPure({ initialState, config, redis });
    expect(initialState).toEqual(eventPure.state);
    await redis.disconnect();
  });
});
