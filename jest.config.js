module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  name: "unit",
  displayName: "unit",
  collectCoverageFrom: ["src/**/*.ts"],
  testMatch: ["<rootDir>/src/**/*.unit.ts", "<rootDir>/src/**/*.int.ts"]
};
