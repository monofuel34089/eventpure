# EventPure

[![Coverage Status](https://coveralls.io/repos/gitlab/monofuel34089/eventpure/badge.svg?branch=master)](https://coveralls.io/gitlab/monofuel34089/eventpure?branch=master)

In Progress! not working yet

- Event Sourcing library written in Typescript

* goals:

  - event validation (io-ts?) - typescript does provide build-time checking of types, however situations like writing events to disk or pushing them over the network might result in mismatching version issues
  - optionally keeping track of state built up from events
  - support multiple instances running in the same app
  - allow running in-memory or backed by redis

# Further Reading

[Event Sourcing](https://martinfowler.com/eaaDev/EventSourcing.html)

# Dependencies

- ioredis is optional
